import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Home, Splash} from '../screens';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabNavigator from './TabNavigator'

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const Hide = {headerShown: false};

const TabNav = () => {
  return (
    <Tab.Navigator tabBar={props => <TabNavigator {...props} />}>
      <Tab.Screen options={Hide} name="Home" component={Home} />
    </Tab.Navigator>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen options={Hide} name="MainApp" component={TabNav} />
        <Stack.Screen options={Hide} name="Home" component={Home} />
        <Stack.Screen options={Hide} name="Splash" component={Splash} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Logo from '../../assets/media/OCLogo.svg';

const Index = ({navigation}) => {
  setTimeout(() => {
    navigation.navigate('MainApp');
  }, 2000);

  return (
    <View style={styles.main}>
      {/* <Logo width={'75%'} height={'100%'} /> */}
    </View>
  );
};

export default Index;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

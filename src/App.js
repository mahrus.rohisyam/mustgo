import React from 'react';
import {StatusBar} from 'react-native';
import MainApp from './router';

const App = () => {
  return (
    <>
      <StatusBar />
      <MainApp />
    </>
  );
};

export default App;
